<%@page import="java.util.List" %>
<%@page import="entities.Contact" %>
<%@page import="entities.ContactGroup" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search results</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
	<% List<Contact> contacts = (List<Contact>) request.getAttribute("list"); 
		if(contacts.size() != 0){%>
		<table>
		<tr><td>Name</td><td>Email</td><td>Address</td><td>Phones</td><td>Groups</td><td>Options</td></tr>
		<% for(Contact c:contacts){ %>
			<tr><td><%= c.getFullName() %></td><td><%=c.getEmail()%></td>
			<td><ul><li><%=c.getAdd().getStreet()%></li><li><% out.print(c.getAdd().getZip()+" "+c.getAdd().getCity());%></li><li><%=c.getAdd().getCountry()%></li></ul></td>
			<td><ul><li><%=c.getPhoneByKind("mobile")%> (M)</li><li><%=c.getPhoneByKind("home")%> (H)</li><li><%=c.getPhoneByKind("work")%> (W)</li></ul></td>
			<td><ul><%for(ContactGroup cg:c.getBooks()){ %>
				<li><%=cg.getGroupName() %></li>
			<%} %></ul></td>
			<td><ul><li><a href="deleteContact.jsp?id=<%=c.getId()%>">Delete Contact</a></li><li><a href="updateContact.jsp?id=<%=c.getId()%>">Update Contact</a></li></ul></td></tr>
			<%}%>
		</table>
		<%} else { %>
			<h4>No Contact Found</h4>
		<%} %>
</body>
</html>