<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create new group</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
	<form method="post" action="NewContactGroupServlet">
		<table>
			<tr>
				<td><h4>Group information</h4></td>
			</tr>
			<tr>
				<td><i>Name: <input type="text" name="group" size="30"></i></td>
			</tr>
			<tr>
				<td><input class="button" type="submit" value="Submit" /><input
					class="button" type="reset" value="Reset"></td>
			</tr>
		</table>
	</form>	
</body>
</html>