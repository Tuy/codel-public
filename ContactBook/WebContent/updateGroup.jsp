<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@ page import="java.util.HashMap"%>
	<%@ page import="java.util.Set"%>
<%@ page import="entities.Contact"%>
<%@ page import="services.ServiceContact"%>
<%@ page import="entities.ContactGroup"%>
<%@ page import="services.ServiceContactGroup"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Group</title>
</head>
<body>
	<jsp:include page="menu.jsp" />
	<form method="post" action="EditContactGroupServlet">
		<input name="id" type="hidden" value="<%=request.getParameter("id")%>" />
		<%
			ServiceContactGroup scg = new ServiceContactGroup();
			ServiceContact sc = new ServiceContact();
			ContactGroup cg = scg.getContactGroup(Long.parseLong(request
					.getParameter("id")));
			if (cg == null)
				response.sendRedirect("listContacts.jsp");
			
			HashMap<Long, Contact> contacts = new HashMap<Long, Contact>();
			
			if(request.getSession().getAttribute("contacts") != null){
				Set<Long> keys = (Set<Long>) request.getSession().getAttribute("contacts");
				for(Long id : keys){
					contacts.put(id, sc.getContact(id));
				}
			}
			else{
				contacts = cg.getHashContacts();
			}
			
			if (request.getParameter("r") != null){
				contacts.remove(Long.parseLong(request
						.getParameter("r")));
			}
			if (request.getParameter("a") != null){
				Long id = Long.parseLong(request.getParameter("a"));
				System.out.println("ADD + "+id);
				contacts.put(id, sc.getContact(id));
			}
			request.getSession().setAttribute("contacts", contacts.keySet());	
			%>
		<table>
			<tr>
				<td><h4>Group informations</h4></td>
			</tr>
			<tr>
				<td><i>Name: </i></td><td><input type="text" name="group" size="30"
						value="<%=cg.getGroupName()%>"></td>
			</tr>
			<tr><td>
			<h4>Contacts:</h4></td></tr>
			<%
				for (long i : contacts.keySet()) {
					Contact c = contacts.get(i);
			%>
			<tr>
				<td><%=c.getFullName()%></td>
				<td><input class="button" type="button" value="Remove"
					onclick="window.location=&quot;updateGroup.jsp?id=<%=request.getParameter("id")%>&r=<%=c.getId()%>&quot;;" /></td>
			</tr>
			<%
				}
			%>
			<tr><td>
				<input list="listContacts" type="text" id="addContact"/>
					<datalist id="listContacts"> <%
					long id = -1;
 				for (Contact c : sc.getAllContacts()) {
 					if(!contacts.containsKey(c.getId())){
 %>					
					<option value="<%=c.getFullName()%>" label="<%=c.getId()%>"/>
					<%
 					}}
					%> 
					</datalist></td><td><input class="button" type="button" value="Add"
					onclick="window.location=&quot;updateGroup.jsp?id=<%=request.getParameter("id")%>&a=<%=id%>&quot;;"/>
			</tr>

			<tr>
				<td><input class="button" type="submit" value="Submit" /></td><td><input
					class="button" type="button" onclick="window.location=&quot;home.jsp&quot;;" value="Cancel" /></td>
			</tr>
		</table>
	</form>
</body>
</html>