<%@ page import="entities.ContactGroup" %>
<%@ page import="services.ServiceContactGroup" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
	<form method="post" action="DeleteGroupServlet">
	<input name="id" type="hidden" value="<%=request.getParameter("id")%>"/>
	<% ServiceContactGroup sc = new ServiceContactGroup();
		ContactGroup cg = sc.getContactGroup(Long.parseLong(request.getParameter("id")));
		if(cg == null) response.sendRedirect("home.jsp");%>
		<table>
			<tr>
				<th><h3>Are you sure you wish to delete the group <%= cg.getGroupName()%>?</h3></th>
			</tr>
			<tr>
				<td><input class="button" type="submit" value="Yes" /></td><td><input
					class="button" type="button" onclick="window.location=&quot;home.jsp&quot;;" value="No" /></td>
			</tr>

		</table>
	</form>
</body>
</html>