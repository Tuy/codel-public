<%@ page import="java.util.List" %>
<%@ page import="entities.ContactGroup" %>
<%@ page import="services.ServiceContactGroup" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create new contact</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
	<form method="post" action="NewContactServlet">
		<table>
			<tr>
				<th><h3>Fill your new contact's informations</h3></th>
			</tr>
				<td><h4>Personal informations</h4></td>
			<tr>
				<td><i>First Name: <input type="text" name="fname"
						size="25"></i></td>
			</tr>
			<tr>
				<td><i>Last Name: <input type="text" name="lname" size="25"></i></td>
			</tr>
			<tr>
				<td><i>Email: <input type="text" name="email" size="30"></i></td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
				<td><h4>Address</h4></td>
			</tr>
			<tr>
				<td><i>Street: <input type="text" name="street" size="30"></i></td>
			</tr>
			<tr>
				<td><i>City: <input type="text" name="city" size="30"></i></td>
			</tr>
			<tr>
				<td><i>Zip: <input type="text" name="zip" size="30"></i></td>
			</tr>
			<tr>
				<td><i>Country: <input type="text" name="country" size="30"></i></td>
			</tr>
			<tr>
				<td><h4>Phone Numbers</h4></td>
			</tr>
			<tr>
				<td><i>Mobile: <input type="text" name="mobilePhone" size="30"></i></td>
			</tr>
			<tr>
				<td><i>Home: <input type="text" name="homePhone" size="30"></i></td>
			</tr>
			<tr>
				<td><i>Work: <input type="text" name="workPhone" size="30"></i></td>
			</tr>
			<tr>
				<td><h4>Groups</h4></td>
			</tr>
			<tr>
				<%ServiceContactGroup sc = new ServiceContactGroup();
				List<ContactGroup> groups = sc.getAllContactGroups();
				for(ContactGroup g:groups){%>
					<td><input type="checkbox" name="groups" value="<%= g.getId() %>"/><%= g.getGroupName() %></td>
				<%}%>
			</tr>
			<tr>
				<td><input class="button" type="submit" value="Submit" /><input
					class="button" type="reset" value="Reset"></td>
			</tr>

		</table>
	</form>
</body>
</html>