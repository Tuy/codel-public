<%@ page import="java.util.List" %>
<%@ page import="entities.Contact" %>
<%@ page import="entities.ContactGroup" %>
<%@ page import="services.ServiceContactGroup" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Groups list</title>
</head>
<body>
	<h3>Group List</h3>
	<% 	ServiceContactGroup sc = new ServiceContactGroup();
		List<ContactGroup> groups = sc.getAllContactGroups(); %>
		<table>
		<tr><td>Name</td><td>Contacts</td><td>Options</td></tr>
		<% for(ContactGroup gc:groups){ %>
			<tr><td><%= gc.getGroupName()%></td>
			<td><ul><%for(Contact c: gc.getContacts()){ %>
				<li><%out.print(c.getFirstName()+" "+c.getLastName());%></li>
			<%}%>
			</ul></td>
			<td><ul><li><a href="deleteGroup.jsp?id=<%=gc.getId()%>">Delete Group</a></li>
			<li><a href="updateGroup.jsp?id=<%=gc.getId()%>">Edit Group</a></li>
			</ul></td></tr>
			<%}%>
		</table>
</body>
</html>