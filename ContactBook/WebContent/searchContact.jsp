<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Search contact</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
	<form method="post" action="FindContactServlet">
		<table>
			<tr>
				<th><h3>Search for a contact</h3></th>
			</tr>
			<tr>
				<td><i>By First Name: <input type="text" name="fname"
						size="25"></i></td>
			</tr>
			<tr>
				<td><i>By Last Name: <input type="text" name="lname"
						size="25"></i></td>
			</tr>
			<tr>
				<td><i>By Email: <input type="text" name="email" size="30"></i></td>
			</tr>
			<tr>
				<td><input class="button" type="submit" value="Submit" /><input
					class="button" type="reset" value="Reset"></td>
			</tr>
		</table>
	</form>
</body>
</html>