<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
</head>
<body>
		<table>
			<tr>
				<th><h2>Welcome <% if(request.getParameter("name") != null) out.print(request.getParameter("name"));%></h2></th>
			<tr>
				<td><a href="addContact.jsp">Add Contact</a></td>
				<td><a href="addGroup.jsp">Add Group</a></td>
				<td><a href="searchContact.jsp">Search Contact</a></td>
				<td><a href="home.jsp">Home</a></td>
			</tr>
		</table>
</body>
</html>