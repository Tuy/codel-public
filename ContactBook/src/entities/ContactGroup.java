package entities;

import java.util.HashMap;
import java.util.Set;


public class ContactGroup{
	private long id;
	private int version;
	private String groupName;
	private Set<Contact> contacts;

	public ContactGroup(){
	}
	
	public ContactGroup(String groupName){
		this.groupName = groupName;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public void addContact(Contact c){
		this.contacts.add(c);
		if(!c.getBooks().contains(this)) c.addBook(this);
	}
	
	public void removeContact(Contact c) {
		this.contacts.remove(c);
		if(c.getBooks().contains(this)) c.removeBook(this);
	}
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}
	
	public HashMap<Long, Contact> getHashContacts(){
		HashMap <Long, Contact> map = new HashMap<Long, Contact>();
		for(Contact c : contacts){
			map.put(c.getId(), c);
		}
		return map;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public boolean isAll(){
		if(groupName.equals("All")) return true;
		else return false;
	}
	
}
