package entities;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;


public class Contact{

	private long id;
	
	private int version;
	private String firstName;
	@NotBlank(message="Last Name cannot be empty")
	private String lastName;
	@Email @NotBlank(message="Email cannot be empty")
	private String email;
	private Address add;
	private Set<ContactGroup> books;
	private Set<PhoneNumber> profiles;

	
	public Contact(){
		books = new HashSet<ContactGroup>();
		profiles = new HashSet<PhoneNumber>();
	}
	
	public Contact(String firstName, String lastName, String email, Address add){
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.add = add;
	}
	
	public void addProfile(PhoneNumber pn){
		profiles.add(pn);
		pn.setContact(this);
	}
	
	public void addBook(ContactGroup cg){
		this.books.add(cg);
		if(!cg.getContacts().contains(this)){
			cg.addContact(this);
		}
	}
	
	public void removeBook(ContactGroup cg) {
		this.books.remove(cg);
		if(cg.getContacts().contains(this)){
			cg.removeContact(this);
		}
		
	}
	
	public Set<ContactGroup> getBooks(){
		books.size();
		return books;
	}
	
	public void setBooks(Set<ContactGroup> books){
		this.books = books;
	}
	
	public Set<PhoneNumber> getProfiles(){
		return profiles;
	}
	
	public boolean isInGroup(long idGroup){
		boolean isContained = false;
		for(ContactGroup cg: books){
			if(cg.getId() == idGroup)
				isContained = true;
		}
		return isContained;
	}
	
	public String getPhoneByKind(String kind){
		String number = "";
		for(PhoneNumber phone: profiles){
			if(phone.getPhoneKind().equals(kind.toLowerCase()))
				number = phone.getPhoneNumber();
		}
		return number;
	}
	
	public void setProfiles(Set<PhoneNumber> phones){
		this.profiles = phones;
	}
	
	public Address getAdd(){
		return add;
	}
	
	public void setAdd(Address address){
		this.add = address;
	}

	public String getEmail(){
		return email;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setFirstName(String firstname){
		this.firstName = firstname;
	}
	
	public String getFullName(){
		return firstName+" "+lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setLastName(String lastname){
		this.lastName = lastname;
	}

	public long getId(){
		return id;
	}

	public void setId(long id){
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
	

}
