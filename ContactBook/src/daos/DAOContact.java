package daos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;












import javax.validation.ConstraintViolationException;

import org.apache.commons.collections.ListUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;












import domain.Messages;
import entities.Address;
import entities.Contact;
import entities.ContactGroup;
import entities.PhoneNumber;

public class DAOContact extends HibernateDaoSupport implements IDAOContact{


	public DAOContact(){
	}

	/**
	 * Rajoute un contact dans la base de donnees.
	 * @param firstname
	 * @param lastname
	 * @param email
	 * @return renvoit le nouveau contact
	 */
	public boolean addContact(Contact contact){
		boolean done = true;
		try{
			this.getHibernateTemplate().persist(contact);
		}catch(ConstraintViolationException e){
			done = false;
		}
		return done;
	}

	/**
	 * Suppresion d'un contact a partir de son identifiant
	 * @param id
	 * @return vrai si la suppression a bien ete effectuee
	 */
	@Transactional
	public boolean deleteContact(long id){
		boolean success = false;

		Contact contact = (Contact) this.getHibernateTemplate().load(Contact.class, id);
		if(contact != null){
			for(ContactGroup cg: contact.getBooks()){
				cg.getContacts().remove(contact);
			}
			this.getHibernateTemplate().delete(contact);
			success = true;
		}
		return success;
	}

	/**
	 * Recuperation d'un contact a partir de son identifiant
	 * @param id
	 * @return
	 */
	@Transactional
	public Contact getContact(long id){
		Contact contact = null;

		Contact c = (Contact) this.getHibernateTemplate().get(Contact.class, id);
		if(c != null){
			contact = c;
		}
		this.getHibernateTemplate().initialize(contact.getBooks());
		return contact;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Contact> simpleSearch(final String field){
		List<Contact> contacts = new ArrayList<Contact>();
		List<Contact> temp;

		temp=(List<Contact>) this.getHibernateTemplate().find("from Contact where firstName=?",field);
		if(temp != null){
			for(Contact c: temp){
				if(!alreadyIn(contacts, c)) contacts.add(c);
			}
		}
		temp= (List<Contact>) this.getHibernateTemplate().execute(new HibernateCallback<List<Contact>>() {
			public List<Contact> doInHibernate(Session session) throws HibernateException,
			SQLException {
				String strbis = '%' + field + '%';
				return session.createCriteria(Contact.class).add(Restrictions.like("lastName", strbis).ignoreCase()).list();
			}
		});
		if(temp != null){
			for(Contact c: temp){
				if(!alreadyIn(contacts, c)) contacts.add(c);
			}
		}

		temp=(List<Contact>) this.getHibernateTemplate().execute(new HibernateCallback<List<Contact>>() {
			public List<Contact> doInHibernate(Session session) throws HibernateException,
			SQLException {
		        Contact c = new Contact();
		        c.setEmail(field);
		        Example exContact = Example.create(c).enableLike(MatchMode.ANYWHERE).ignoreCase().excludeZeroes();
		        return session.createCriteria(Contact.class).add(exContact).list();
			}
			});
		if(temp != null){
			for(Contact c: temp){
				if(!alreadyIn(contacts, c)) contacts.add(c);
			}
		}




		temp = (List<Contact>)this.getHibernateTemplate().execute(new HibernateCallback(){
			public Object doInHibernate(Session session) throws	HibernateException{

				List<Contact> adds = (List<Contact>) session.createQuery("from Contact where add.street='"+field+"' or add.city='"+field+"' or add.country='"+field+"'").list();
				return adds;
			}
		});

		if(temp != null){
			for(Contact c: temp){
				if(!alreadyIn(contacts, c)) contacts.add(c);
			}
		}
		return contacts;
	}


	@SuppressWarnings("unchecked")
	@Transactional
	public List<Contact> searchContacts(String fname, String lname, String email, String street, String city, String zip, String country){
		List<Contact> contacts;

		boolean empty=true, first=true;
		String query = "from Contact";
		String fields="";
		if(!fname.equals("")){
			empty=false;
			fields = " firstName='"+fname+"'";
			first = false;
		}
		if(!lname.equals("")){
			empty=false;
			if(!first){
				fields += " and";
				first = false;
			}
			fields += " lastName='"+lname+"'";
		}
		if(!email.equals("")){
			empty=false;
			if(!first){
				fields += " and";
				first = false;
			}
			fields += " email='"+email+"'";
		}
		if(!street.equals("")){
			empty=false;
			if(!first){
				fields += " and";
				first = false;
			}
			fields += " add.street='"+street+"'";
		}
		if(!city.equals("")){
			empty=false;
			if(!first){
				fields += " and";
				first = false;
			}
			fields += " add.city='"+city+"'";
		}
		if(!zip.equals("")){
			empty=false;
			if(!first){
				fields += " and";
				first = false;
			}
			fields += " add.zip='"+zip+"'";
		}
		if(!country.equals("")){
			empty=false;
			if(!first){
				fields += " and";
				first = false;
			}
			fields += " add.country='"+country+"'";
		}


		if(!empty){
			query += " where"+fields;
		}
		contacts = (List<Contact>) this.getHibernateTemplate().find(query);
		return contacts;
	}

	/**
	 * Methode qui modifie les coordonees d'un contact
	 * @param id
	 * @param firstname
	 * @param alstname
	 * @param email
	 * @return
	 */
	public void modifyContact(Contact c){
		try{
		this.getHibernateTemplate().update(c);
		}
		catch(Exception e){
			
		}
	}

	/**
	 * Renvoit la liste des contacts correspondant au prenom firstname
	 * @param firstname
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Contact> getContactByFirstName(String firstname){
		List<Contact> contacts;
		contacts = (List<Contact>) this.getHibernateTemplate().find("from Contact where firstName=?",firstname);
		return contacts;
	}


	/**
	 * Renvoit la liste des contacts correspondant au nom lastname
	 * @param lastname
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Contact> getContactByLastName(String lastname){
		List<Contact> contacts;
		contacts = (List<Contact>) this.getHibernateTemplate().find("from Contact where lastName=?",lastname);
		return contacts;
	}

	/**
	 * Renvoit la liste des contacts correspondant a l'email email
	 * @param email
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Contact> getContactByEmail(String email){
		List<Contact> contacts;
		contacts = (List<Contact>) this.getHibernateTemplate().find("from Contact where email="+email);
		return contacts;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Contact> getAllContacts(){
		List<Contact> contacts;
		contacts = (List<Contact>) this.getHibernateTemplate().find("from Contact");
		return contacts;
	}

	public void updatePhone(Contact c, String kind, String number){
		for(PhoneNumber pn : c.getProfiles()){
			if(pn.getPhoneKind().equals(kind)){
				pn.setPhoneNumber(number);
				this.getHibernateTemplate().saveOrUpdate(pn);
				return;
			}
		}		
	}
	public void deletePhone(Contact c, String kind){
		for(PhoneNumber pn : c.getProfiles()){
			if(pn.getPhoneKind().equals(kind)){
				c.getProfiles().remove(pn);
				this.getHibernateTemplate().saveOrUpdate(c);
				return;
			}
		}
	}

	private boolean alreadyIn(List<Contact> res, Contact tmp){
		for(Contact c : res){
			if(c.getId() == tmp.getId()) return true;
		}
		return false;
	}

}
