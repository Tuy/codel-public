package daos;

import java.util.List;

import entities.Contact;

public interface IDAOContact {

	
	public boolean addContact(Contact c);
	
	public boolean deleteContact(long id);
	
	public Contact getContact(long id);
	
	public void modifyContact(Contact c);
	
	public List<Contact> getContactByFirstName(String firstname);
	
	public List<Contact> getContactByLastName(String lastname);
	
	public List<Contact> getContactByEmail(String email);

	public List<Contact> getAllContacts();

	public List<Contact> searchContacts(String fname, String lname,
			String email, String street, String city, String zip,
			String country);
	
	public List<Contact> simpleSearch(String field);
	
	public void updatePhone(Contact c, String kind, String number);
	
	public void deletePhone(Contact c, String kind);
	
}
