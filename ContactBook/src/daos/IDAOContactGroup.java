package daos;

import java.util.List;

import entities.ContactGroup;

public interface IDAOContactGroup {

	public void addGroupContact(ContactGroup group);
	
	public List<ContactGroup> getAllContactGroups();
	
	public List<ContactGroup> getContactGroups();
	
	public ContactGroup getContactGroup(long id);

	public void removeContactFromGroup(long idContact, long idGroup);

	public void addContactToGroup(long idContact, long idGroup);

	public boolean deleteGroup(long id);
	
	public void modifyContactGroup(ContactGroup cg);
	
	public ContactGroup getContactGroup(String name);
}
