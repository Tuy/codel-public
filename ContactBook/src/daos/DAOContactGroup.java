package daos;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import entities.Contact;
import entities.ContactGroup;

public class DAOContactGroup extends HibernateDaoSupport implements IDAOContactGroup {

	@Override
	public void addGroupContact(ContactGroup group) {
		this.getHibernateTemplate().persist(group);
	}

	public void modifyContactGroup(ContactGroup group){
		try{
		this.getHibernateTemplate().update(group);
		}
		catch(Exception e){
			
		}
	}
	
	@Transactional
	public void addContactToGroup(long idContact, long idGroup) {
        Contact c = (Contact) this.getHibernateTemplate().get(Contact.class, idContact);
        ContactGroup cg = (ContactGroup) this.getHibernateTemplate().get(ContactGroup.class, idGroup);
		cg.addContact(c);		
	}

	@Transactional
	public void removeContactFromGroup(long idContact, long idGroup) {
        Contact c = (Contact) this.getHibernateTemplate().get(Contact.class, idContact);
        ContactGroup cg = (ContactGroup) this.getHibernateTemplate().get(ContactGroup.class, idGroup);
		cg.removeContact(c);
	}

	@Transactional
	public boolean deleteGroup(long id) {
		boolean success = false;

        ContactGroup group = (ContactGroup) this.getHibernateTemplate().get(ContactGroup.class, id);
        if(group != null){
        	for(Contact c: group.getContacts()) c.getBooks().remove(group);     
        	this.getHibernateTemplate().delete(group);
        	success = true;
        }
        return success;
		
	}

	@SuppressWarnings("unchecked")
	public List<ContactGroup> getAllContactGroups() {
		return (List<ContactGroup>) this.getHibernateTemplate().find("from ContactGroup");
	}
	
	@SuppressWarnings("unchecked")
	public List<ContactGroup> getContactGroups() {
		return (List<ContactGroup>) this.getHibernateTemplate().find("from ContactGroup where groupName!='All'");
	}

	public ContactGroup getContactGroup(long id) {
		ContactGroup cgroup = null;
        
        ContactGroup cg = (ContactGroup) this.getHibernateTemplate().get(ContactGroup.class, id);
        if(cg != null){
        	cgroup = cg;
        }

        this.getHibernateTemplate().initialize(cg.getContacts());
		return cgroup;
	}
	
	public ContactGroup getContactGroup(String name){
		return (ContactGroup) this.getHibernateTemplate().find("from ContactGroup where groupName='"+name+"'").get(0);
	}


}
