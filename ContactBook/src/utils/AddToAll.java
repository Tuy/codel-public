package utils;

import org.aspectj.lang.JoinPoint;

import services.ServiceContactGroup;
import entities.Contact;
import entities.ContactGroup;

public class AddToAll {
	public void addToAll(JoinPoint joinPoint){
		Contact c = (Contact) joinPoint.getArgs()[0];
		ServiceContactGroup sg = new ServiceContactGroup();
		if(!sg.alreadyExist("All", -1)){
			sg.createContactGroup("All");
		}
		ContactGroup cg = sg.getContactGroup("All");
		sg.addContactToGroup(c.getId(), cg.getId());
	}
}
