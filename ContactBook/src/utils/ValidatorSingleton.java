package utils;

import javax.validation.Validation;
import javax.validation.ValidatorFactory;

public class ValidatorSingleton {
	private static ValidatorFactory factory = null;
	
	public static ValidatorFactory getFactory(){
		if(factory == null)
			factory = Validation.buildDefaultValidatorFactory();
		return factory;
	}
}
