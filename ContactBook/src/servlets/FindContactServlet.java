package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.ServiceContact;
import daos.DAOContact;
import entities.Contact;

/**
 * Servlet implementation class FindContactServlet
 */
public class FindContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindContactServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String email = request.getParameter("email");
		
		ServiceContact sc = new ServiceContact();
		List<Contact> results = sc.search(fname, lname, email, "", "", "", "");
		
		
		System.out.println("--------------------------------"+results);
		if(results != null){
			RequestDispatcher rd = request.getRequestDispatcher("contactsFound.jsp");
			request.setAttribute("list", results);
			rd.forward(request, response);
		}
		else
			response.sendRedirect("home.jsp");
	}
	

}
