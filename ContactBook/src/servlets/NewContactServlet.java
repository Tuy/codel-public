package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import services.ServiceContact;
import daos.DAOContact;

/**
 * Servlet implementation class newContactServlet
 */
public class NewContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewContactServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String email = request.getParameter("email");
		
		String street = request.getParameter("street");
		String city = request.getParameter("city");
		String zip = request.getParameter("zip");
		String country = request.getParameter("country");

		String mobile = request.getParameter("mobilePhone");
		String home = request.getParameter("homePhone");
		String work = request.getParameter("workPhone");
		
		String[] groups = request.getParameterValues("groups");
		long[] idGroups;
		if(groups == null){
			idGroups = new long[1];
			idGroups[0]=-1;
		}
		else{
			idGroups = new long[groups.length];
			for(int i=0; i<groups.length; i++){
				if(!groups[i].equals("")) idGroups[i] = Long.parseLong(groups[i]);
				else idGroups[i] = -1;
			}
		}
		ServiceContact sc = new ServiceContact();
		sc.createContact(fname, lname, email, street, city, zip, country, mobile, home, work, idGroups, null);
		
		response.sendRedirect("home.jsp");
	}

}
