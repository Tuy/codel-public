package servlets;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.Contact;
import services.ServiceContact;
import services.ServiceContactGroup;

/**
 * Servlet implementation class EditContactGroupServlet
 */
public class EditContactGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditContactGroupServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("group");
		Set<Long> c = (Set<Long>) request.getSession().getAttribute("contacts");
		long id = Long.parseLong(request.getParameter("id"));
		ServiceContact sc = new ServiceContact();
		
		request.getSession().invalidate();
		
		System.out.println("-------------------------------------------------------------------------------------------------------------"+name);
		
		Set<Contact> contacts = new HashSet<Contact>();
		for(Long ids : c){
			contacts.add(sc.getContact(ids));
		}
		
		
		for(Contact test : contacts){
			System.out.println(test.getFullName());
		}
		
		ServiceContactGroup scg = new ServiceContactGroup();
		//scg.editContactGroup(id, name, contacts);
		response.sendRedirect("home.jsp");
	}

}
