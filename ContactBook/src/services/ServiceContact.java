package services;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import utils.AppContextSingleton;
import utils.ValidatorSingleton;
import daos.IDAOContact;
import entities.Address;
import entities.Contact;
import entities.ContactGroup;
import entities.Entreprise;
import entities.PhoneNumber;

public class ServiceContact{
	
	public void testValidator(){
		ValidatorFactory factory = (ValidatorFactory) ValidatorSingleton.getFactory();
		Contact c = new Contact("first", "", "monmail", null);
		
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Contact>> constraintValidator = validator.validate(c);
		System.out.println(constraintValidator.size());
	}

	public boolean createContact(String fname, String lname, String email, String street, String city, String zip, String country, String mobile, String home, String work, long[] idGroups, String siret){
		boolean done = false;
		
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");

		Contact c;
		
		Address add = new Address();
		add.setCity(city);
		add.setCountry(country);
		add.setStreet(street);
		add.setZip(zip);
		
		if(siret == null) c = new Contact();
		else{
			c = new Entreprise();
			((Entreprise)c).setSiret(siret);
		}
		
		c.setFirstName(fname);
		c.setLastName(lname);
		c.setEmail(email);		
		c.setAdd(add);
		
		if(!mobile.equals("")){
			c.addProfile(new PhoneNumber("mobile", mobile));
		}
		if(!home.equals(""))
			c.addProfile(new PhoneNumber("home", home));
		if(!work.equals(""))
			c.addProfile(new PhoneNumber("work", work));
		
		if(dao.addContact(c)) done = true;
		
		for(long id: idGroups){
			if(id != -1){
				ServiceContactGroup sc = new ServiceContactGroup();
				sc.addContactToGroup(c.getId(), id);
			}
		}
		return done;
	}
	
	public boolean createContact(Contact c){
		boolean done = false;
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		if(dao.addContact(c)) done = true;
		return done;
	}
	
	public void editContact(long id, String fname, String lname, String email, String street, String city, String zip, String country, String mobile, String home, String work, long[] idGroups, String siret){

		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");

		Contact c = dao.getContact(id);
		
		if(siret != null){
			((Entreprise)c).setSiret(siret);
		}
		
		if(c != null){
			if(siret != null){
				((Entreprise)c).setSiret(siret);
			}
			else{
				if(!home.equals(""))
					dao.updatePhone(c, "home", home);
				else
					dao.deletePhone(c, "home");
			}
			
			Address add = new Address();
			add.setCity(city);
			add.setCountry(country);
			add.setStreet(street);
			add.setZip(zip);

			c.setFirstName(fname);
			c.setLastName(lname);
			c.setEmail(email);		
			c.setAdd(add);
			
			if(!mobile.equals(""))
				dao.updatePhone(c, "mobile", mobile);
			else
				dao.deletePhone(c, "mobile");
			if(!work.equals(""))
				dao.updatePhone(c, "work", work);
			else
				dao.deletePhone(c, "work");
			
			
			for(ContactGroup cg: c.getBooks()){
				boolean keep = false;
				for(long idG: idGroups){
					if(cg.getId() == idG) keep = true;				
				}
				if(!keep){
					ServiceContactGroup sc = new ServiceContactGroup();
					sc.removeContactFromGroup(c.getId(), cg.getId());
				}
			}
			dao.modifyContact(c);

			for(long idG: idGroups){
				if((idG != -1) && (!c.isInGroup(idG))){
					ServiceContactGroup sc = new ServiceContactGroup();
					sc.addContactToGroup(c.getId(), idG);
				}
			}
			
		}
	}

	public void deleteContact(long id){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		dao.deleteContact(id);
	}
	
	public List<Contact> searchByFirstName(String fname){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		return dao.getContactByFirstName(fname);
	}
	
	public List<Contact> searchByLastName(String lname){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		return dao.getContactByLastName(lname);
	}
	
	public List<Contact> searchByEmail(String email){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		return dao.getContactByEmail(email);
	}
	
	public List<Contact> search(String fname, String lname, String email, String street, String city, String zip, String country){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		return dao.searchContacts(fname, lname, email, street, city, zip, country);		
	}
	
	public List<Contact> simpleSearch(String simple){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		List<Contact> l = dao.simpleSearch(simple);
		return l;
	}
	
	public List<Contact> getAllContacts(){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		return dao.getAllContacts();
	}
	
	public Contact getContact(long id){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		return dao.getContact(id);
	}
	
	public int getNbContacts(){
		IDAOContact dao = (IDAOContact) AppContextSingleton.getContext().getBean("DAOC");
		return dao.getAllContacts().size();
		
	}
	
	
}
