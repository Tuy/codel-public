package services;

import java.util.List;
import java.util.Set;


import utils.AppContextSingleton;
import daos.IDAOContactGroup;
import entities.Contact;
import entities.ContactGroup;

public class ServiceContactGroup{


	public void createContactGroup(String group) {
		ContactGroup cgroup = new ContactGroup();
		cgroup.setGroupName(group);

		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		dao.addGroupContact(cgroup);
	}
	
	public void editContactGroup(Long id, String name, Set<Contact> contacts){
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		ContactGroup cg = dao.getContactGroup(id);
		
		if(!cg.getGroupName().equals(name)){
			cg.setGroupName(name);			
		}
		
		for(Contact c: cg.getContacts()){
			boolean keep = false;
			for(Contact co : contacts){
				if(c.getId() == co.getId()) keep = true;
			}
			if(!keep){
				dao.removeContactFromGroup(c.getId(), cg.getId());				
			}
		}
		
		dao.modifyContactGroup(cg);
		for(Contact c : contacts){
			if(!c.isInGroup(id)){
				dao.addContactToGroup(c.getId(), id);
			}
		}
		
	}

	public void deleteGroup(long id) {
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		dao.deleteGroup(id);
		
	}
	
	public List<ContactGroup> getAllContactGroups(){
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		return dao.getAllContactGroups();
	}
	
	public List<ContactGroup> getContactGroups(){
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		return dao.getContactGroups();
		
	}

	public void addContactToGroup(long idContact, long idGroup) {
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		dao.addContactToGroup(idContact, idGroup);
		
	}

	public void removeContactFromGroup(long idContact, long idGroup) {
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		dao.removeContactFromGroup(idContact, idGroup);		
	}
	
	public ContactGroup getContactGroup(long id){
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		return dao.getContactGroup(id);
	}

	public ContactGroup getContactGroup(String name){
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		return dao.getContactGroup(name);
	}

	public int getNbGroups(){
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		return dao.getAllContactGroups().size();

	}
	
	public boolean alreadyExist(String newGroup, long id){
		boolean exists = false;
		IDAOContactGroup dao = (IDAOContactGroup) AppContextSingleton.getContext().getBean("DAOCG");
		for(ContactGroup cg : dao.getAllContactGroups()){
			if((cg.getGroupName().toLowerCase().equals(newGroup.toLowerCase())) &&
				(cg.getId() != id))
				exists = true;
		}
		return exists;
	}

}
