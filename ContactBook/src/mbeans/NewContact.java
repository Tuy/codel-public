package mbeans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.faces.bean.ManagedBean;

import services.ServiceContact;
import entities.Address;
import entities.Contact;
import entities.ContactGroup;
import entities.PhoneNumber;

@ManagedBean
public class NewContact implements Serializable {

	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private Address add = new Address();
	private Set<ContactGroup> books;
	private Set<PhoneNumber> profiles;
	
	private String numberM, numberH, numberW;
	private long[] ids=null;
	private String siret;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getAdd() {
		return add;
	}
	public void setAdd(Address add) {
		this.add = add;
	}
	public Set<ContactGroup> getBooks() {
		return books;
	}
	public void setBooks(Set<ContactGroup> books) {
		this.books = books;
	}
	public Set<PhoneNumber> getProfiles() {
		return profiles;
	}
	public void setProfiles(Set<PhoneNumber> profiles) {
		this.profiles = profiles;
	}
	public long[] getIds(){
		return this.ids;
	}
	public void setIds(long[] ids){
		this.ids = ids;
	}
	
	public String getNumberM() {
		return numberM;
	}
	public void setNumberM(String numberM) {
		this.numberM = numberM;
	}
	public String getNumberH() {
		return numberH;
	}
	public void setNumberH(String numberH) {
		this.numberH = numberH;
	}
	public String getNumberW() {
		return numberW;
	}
	public void setNumberW(String numberW) {
		this.numberW = numberW;
	}
	public String getSiret(){
		return siret;
	}
	public void setSiret(String siret){
		this.siret = siret;
	}
		
	public String addContact(){		
		
		if(this.ids==null){
			ids = new long[1];
			ids[0] = -1;
		}
		ServiceContact sc = new ServiceContact();
		if(sc.createContact( this.firstName ,this.lastName, this.email, this.add.getStreet(), this.add.getCity(), this.add.getZip(), this.add.getCountry(), numberM, numberH, numberW, ids, siret))
			return "done";
		else
			return "error4";
	}
	
}
