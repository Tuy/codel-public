package mbeans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import services.ServiceContact;
import entities.Address;
import entities.Contact;
import entities.ContactGroup;
import entities.PhoneNumber;

@ManagedBean
@ViewScoped
public class DeleteContact implements Serializable {

	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private Address add;
	private Set<ContactGroup> books;
	private Set<PhoneNumber> profiles;
	
	public DeleteContact(){
		String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("contactID");
		if(param != null){
			this.id = Long.parseLong(param);
			ServiceContact sg = new ServiceContact();
			Contact c = sg.getContact(this.id);
			this.firstName = c.getFirstName();
			this.lastName = c.getLastName();
			this.email = c.getEmail();
			this.add=c.getAdd();
			this.books=c.getBooks();
			this.profiles=c.getProfiles();
			
		}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getAdd() {
		return add;
	}
	public void setAdd(Address add) {
		this.add = add;
	}
	public Set<ContactGroup> getBooks() {
		return books;
	}
	public void setBooks(Set<ContactGroup> books) {
		this.books = books;
	}
	public Set<PhoneNumber> getProfiles() {
		return profiles;
	}
	public void setProfiles(Set<PhoneNumber> profiles) {
		this.profiles = profiles;
	}
	public String getFullName(){
		return this.getFirstName()+" "+this.getLastName();
	}
	
	public String deleteContact(){

		ServiceContact sc = new ServiceContact();
		sc.deleteContact(this.id);
		
		return "done";
	}
}
