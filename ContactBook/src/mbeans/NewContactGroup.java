package mbeans;

import java.io.Serializable;
import java.util.Set;

import javax.faces.bean.ManagedBean;

import services.ServiceContactGroup;
import entities.Contact;

@ManagedBean
public class NewContactGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String groupName;
	private Set<Contact> contacts;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Set<Contact> getContacts() {
		return contacts;
	}
	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public String addGroup(){
		ServiceContactGroup sc = new ServiceContactGroup();
		if(!sc.alreadyExist(groupName, this.id))
			sc.createContactGroup(this.groupName);
		else
			return "error1";
		
		return "done";
	}
}
