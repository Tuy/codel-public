package mbeans;

import javax.faces.bean.ManagedBean;

import services.ServiceContact;
import utils.AppContextSingleton;
import daos.IDAOContact;
import entities.Contact;

@ManagedBean
public class Generator {

	private Contact c1;
	private Contact c2;
	
	
	public String generateContacts(){
		ServiceContact sc = new ServiceContact();
		c1 = (Contact) AppContextSingleton.getContext().getBean("DATA1");
		c2 = (Contact) AppContextSingleton.getContext().getBean("DATA2");
		
		if(sc.createContact(c1) && sc.createContact(c2))
			return "doneGenerate";
		else return "errorGenerate";
	}
	
	
	public String testValidator(){
		ServiceContact sc = new ServiceContact();
		sc.testValidator();
		return "home";
	}

	public Contact getC1() {
		return c1;
	}
	public void setC1(Contact c1) {
		this.c1 = c1;
	}
	public Contact getC2() {
		return c2;
	}
	public void setC2(Contact c2) {
		this.c2 = c2;
	}
	
	
}
