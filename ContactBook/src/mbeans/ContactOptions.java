package mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import entities.Address;
import entities.Contact;
import entities.ContactGroup;
import entities.Entreprise;
import entities.PhoneNumber;
import services.ServiceContact;
import services.ServiceContactGroup;
@ManagedBean
public class ContactOptions implements Serializable{

	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private Address add;
	private Set<ContactGroup> books;
	private Set<PhoneNumber> profiles;
	private String numberM, numberH, numberW;
	private long[] ids=null;
	private String siret = null;

	public ContactOptions(){
		String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("contactID");
		if(param != null){
			this.id = Long.parseLong(param);
			ServiceContact sc = new ServiceContact();
			Contact c = sc.getContact(this.id);
			this.firstName = c.getFirstName();
			this.lastName = c.getLastName();
			this.email = c.getEmail();
			this.add = c.getAdd();
			this.books = c.getBooks();
			this.profiles = c.getProfiles();
			for(PhoneNumber pn : profiles){
				if(pn.getPhoneKind().equals("mobile")){
					this.numberM=pn.getPhoneNumber();
				
				}else if (pn.getPhoneKind().equals("home")){
						this.numberH=pn.getPhoneNumber();
					
				}else{
					if(pn.getPhoneKind().equals("work")){
						this.numberW=pn.getPhoneNumber();
					}
				}
			}
			this.ids = new long[this.books.size()];
			int i=0;
			for(ContactGroup cg : this.books){
				ids[i] = cg.getId();
				i++;
			}
			if(c instanceof Entreprise){
				this.siret = ((Entreprise)c).getSiret();
			}
		}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getAdd() {
		return add;
	}
	public void setAdd(Address add) {
		this.add = add;
	}
	public Set<ContactGroup> getBooks() {
		return books;
	}
	public void setBooks(Set<ContactGroup> books) {
		this.books = books;
	}
	public Set<PhoneNumber> getProfiles() {
		return profiles;
	}
	public void setProfiles(Set<PhoneNumber> profiles) {
		this.profiles = profiles;
	}
	public long[] getIds(){
		return this.ids;
	}
	public void setIds(long[] ids){
		this.ids = ids;
	}
	
	public String getNumberM() {
		return numberM;
	}
	public void setNumberM(String numberM) {
		this.numberM = numberM;
	}
	public String getNumberH() {
		return numberH;
	}
	public void setNumberH(String numberH) {
		this.numberH = numberH;
	}
	public String getNumberW() {
		return numberW;
	}
	public void setNumberW(String numberW) {
		this.numberW = numberW;
	}
	public String getSiret(){
		return siret;
	}
	public void setSiret(String siret){
		this.siret = siret;
	}
	


	public int getNbContacts(){
		ServiceContact sc = new ServiceContact();
		return sc.getNbContacts();
	}
	
	public List<Contact> getAllContacts(){
		ServiceContact sc = new ServiceContact();
		return sc.getAllContacts();
	}

	
	public List<ContactGroup> getGroupsOfContact(){
		if(this.id > 0){
			ServiceContact sc = new ServiceContact();
			return new ArrayList<ContactGroup>(sc.getContact(this.id).getBooks());
		}
		else return null;		
	}
}
