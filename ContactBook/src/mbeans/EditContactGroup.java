package mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import services.ServiceContact;
import services.ServiceContactGroup;
import entities.Contact;
import entities.ContactGroup;

@ManagedBean
@ViewScoped
public class EditContactGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id = 0;
	private String groupName;
	private Set<Contact> contacts;
	private String addContact;
	private int version;

	public EditContactGroup(){
		String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("groupID");
		if(param != null){
			this.id = Long.parseLong(param);
			ServiceContactGroup sg = new ServiceContactGroup();
			ContactGroup cg = sg.getContactGroup(this.id);
			this.groupName = cg.getGroupName();
			this.contacts = cg.getContacts();
			this.version = cg.getVersion();
		}
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Set<Contact> getContacts() {
		return contacts;
	}
	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}	
	public String getAddContact() {
		return addContact;
	}
	public void setAddContact(String addContact) {
		this.addContact = addContact;
	}

	

	public String editGroup(){
		ServiceContactGroup sc = new ServiceContactGroup();
		if(this.version!=sc.getContactGroup(this.id).getVersion()){
			return "error3";
		}
		if(sc.alreadyExist(groupName, this.id))
			return "error2";
		sc.editContactGroup(this.id, this.groupName, this.contacts);
		
		return "done";
	}
	
	public void addContact(){
		ServiceContactGroup sg = new ServiceContactGroup();
		ServiceContact sc = new ServiceContact();
		Contact c = null;
		if(addContact != null){
			c = sc.getContact(Long.parseLong(addContact));
		}

		if((this.id > 0) && (c != null)){
			this.contacts.add(c);
		}

		
	}
	
	public void removeContact(String id){
		System.out.println("------------REMOVE");
		ServiceContactGroup sg = new ServiceContactGroup();
		ServiceContact sc = new ServiceContact();
		Contact c = null;
		if(id != null){
			c = sc.getContact(Long.parseLong(id));
		}

		if((this.id > 0) && (c != null)){
			Contact con = null;
			for(Contact co : contacts){
				if(co.getId() == c.getId()){
					con = co;
					break;
				}
			}
			if(con != null)
				contacts.remove(con);
		}
	}
	
	public boolean noContactsLeft(){
		if(getOtherContacts().size() == 0)
			return true;
		else return false;
	}
	
	public List<Contact> getContactsOfGroup(){
		if(this.id > 0){
			return new ArrayList<Contact>(contacts);
		}
		else return null;		
	}
	
	public List<Contact> getOtherContacts(){
		ServiceContact sc = new ServiceContact();
		List<Contact> rest = sc.getAllContacts();
		for(Contact c : sc.getAllContacts()){
			if(isInHere(c.getId())){
				remove(rest, c.getId());
			}
		}
		return rest;
	}
	
	private boolean isInHere(long id){
		boolean exists = false;
		for(Contact c : contacts){
			if(c.getId() == id){
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	private void remove(List<Contact> list, long id){
		Contact c = null;
		for(Contact co: list){
			if(co.getId() == id) c=co;
		}
		if(c != null) list.remove(c);
	}
}
