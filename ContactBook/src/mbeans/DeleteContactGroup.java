package mbeans;

import java.io.Serializable;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import services.ServiceContactGroup;
import entities.Contact;

@ManagedBean
@ViewScoped
public class DeleteContactGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String groupName;
	private Set<Contact> contacts;

	public DeleteContactGroup(){
		String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("groupID");
		if(param != null){
			this.id = Long.parseLong(param);
			ServiceContactGroup sg = new ServiceContactGroup();
			this.groupName = sg.getContactGroup(this.id).getGroupName();
			this.contacts = sg.getContactGroup(this.id).getContacts();
		}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Set<Contact> getContacts() {
		return contacts;
	}
	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public String deleteGroup(){
		ServiceContactGroup sc = new ServiceContactGroup();
		sc.deleteGroup(this.id);
		return "done";
	}
}
