package mbeans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.hibernate.jmx.StatisticsService;

import utils.AppContextSingleton;

@ManagedBean
@ApplicationScoped
public class Statistics {
	private StatisticsService stats;
	
	public Statistics(){
		this.stats = (StatisticsService)AppContextSingleton.getContext().getBean("hibernateStatisticsBean");
	}

	public StatisticsService getStats() {
		return stats;
	}

	public void setStats(StatisticsService stats) {
		this.stats = stats;
	}
	
	
}
