package mbeans;	

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import services.ServiceContact;
import entities.Address;
import entities.Contact;
import entities.ContactGroup;
import entities.PhoneNumber;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

@ManagedBean
@SessionScoped
public class FindContact implements Serializable{

	private static final long serialVersionUID = 1L;
	private String defaultSearch;
	private Map<String, String> advancedSearch;
	private List<Contact> results;

	private String firstName;
	private String lastName;
	private String email;
	private Address add = new Address();
	private Set<ContactGroup> books;
	private Set<PhoneNumber> profiles;
	private String numberM, numberH, numberW;
	private long[] ids=null;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getAdd() {
		return add;
	}
	public void setAdd(Address add) {
		this.add = add;
	}
	public Set<ContactGroup> getBooks() {
		return books;
	}
	public void setBooks(Set<ContactGroup> books) {
		this.books = books;
	}
	public Set<PhoneNumber> getProfiles() {
		return profiles;
	}
	public void setProfiles(Set<PhoneNumber> profiles) {
		this.profiles = profiles;
	}
	public String getNumberM() {
		return numberM;
	}
	public void setNumberM(String numberM) {
		this.numberM = numberM;
	}
	public String getNumberH() {
		return numberH;
	}
	public void setNumberH(String numberH) {
		this.numberH = numberH;
	}
	public String getNumberW() {
		return numberW;
	}
	public void setNumberW(String numberW) {
		this.numberW = numberW;
	}
	public long[] getIds() {
		return ids;
	}
	public void setIds(long[] ids) {
		this.ids = ids;
	}
	public String getDefaultSearch() {
		return defaultSearch;
	}
	public void setDefaultSearch(String defaultSearch) {
		this.defaultSearch = defaultSearch;
	}
	public Map<String, String> getAdvancedSearch() {
		return advancedSearch;
	}
	public void setAdvancedSearch(Map<String, String> advancedSearch) {
		this.advancedSearch = advancedSearch;
	}
	public List<Contact> getResults() {
		return results;
	}
	public void setResults(List<Contact> results) {
		this.results = results;
	}
	public String simpleSearch(){
		if(defaultSearch.equals("")){ /*OU check null*/
			return "search";
		}
		else{
			ServiceContact sc = new ServiceContact();
			results = sc.simpleSearch(this.defaultSearch);
			return "searchResults";
		}
	}
	
	public String advancedSearch(){
		ServiceContact sc = new ServiceContact();
		results = sc.search(this.firstName, this.lastName, this.email, this.add.getStreet(), this.add.getCity(), this.add.getZip(), this.add.getCountry());
		return "searchResults";
	}
	
}
