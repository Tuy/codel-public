package mbeans;

import services.ServiceContactGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import entities.Contact;
import entities.ContactGroup;


@ManagedBean
public class ContactGroupOptions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id = 0;
	private String groupName;
	private Set<Contact> contacts;

	public ContactGroupOptions(){
		String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("groupID");
		if(param != null){
			this.id = Long.parseLong(param);
			ServiceContactGroup sg = new ServiceContactGroup();
			ContactGroup cg = sg.getContactGroup(id);
			this.groupName = cg.getGroupName();
			this.contacts = cg.getContacts();
		}
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Set<Contact> getContacts() {
		return contacts;
	}
	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public int getNbGroups(){
		ServiceContactGroup sg = new ServiceContactGroup();
		return sg.getNbGroups();
	}
		
	public List<ContactGroup> getContactGroups(){
		ServiceContactGroup sg = new ServiceContactGroup();
		return sg.getContactGroups();
	}
	
	public List<ContactGroup> getAllContactGroups(){
		ServiceContactGroup sg = new ServiceContactGroup();
		return sg.getAllContactGroups();
	}
	
	public List<Contact> getContactsOfGroup(){
		if(this.id > 0){
			ServiceContactGroup sg = new ServiceContactGroup();
			return new ArrayList<Contact>(sg.getContactGroup(this.id).getContacts());
		}
		else return null;		
	}
	
	public boolean isAll(){
		if(groupName.equals("All")) return true;
		else return false;
	}
}
